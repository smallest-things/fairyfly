const mqtt = require('mqtt')
const Maybe = require('monet').Maybe
const Validation = require('monet').Validation
const yaml = require('js-yaml')
const fs = require('fs')

function getConfig(configFile) {
  try {
    return Validation.Success(yaml.load(fs.readFileSync(configFile, 'utf8')))
  } catch (error) {
    return Validation.Fail(error.message)
  }
}

function createMqttSubscriber(member, subscriber) {
  let mqtt_client  = mqtt.connect(subscriber.mqtt_url)

  mqtt_client.on('connect', _ => {
    mqtt_client.subscribe(subscriber.topic, (error) => {
      console.log(error ? `😡 ${error.message}` : `😃 ${member} subscribed to ${subscriber.topic} topic`)
    })
  })
  mqtt_client.on('message',  (topic, message) => {
    console.log(`${member}`, message.toString())

    fs.appendFile(`${member}.log`, `${new Date()};${message.toString()}\n`, 'utf8', () => {
      //foo
    })


  })
}

function createMqttPublisher(member, publisher) {
  let mqtt_client  = mqtt.connect(publisher.mqtt_url)

  mqtt_client.on('connect', _ => {
    setInterval(function() {
      mqtt_client.publish(publisher.topic, publisher.message)
    }, publisher.interval)
  })
}


let config  = getConfig('config.yaml').cata(
  error_message => {
    console.error("😡", error_message)
    process.exit()
  },
  config_content => {
    console.log("😃 file exists", config_content)
    return Maybe.fromUndefined(config_content)
  }
)

let subscribers = config.cata(
  _ => {
    console.error("😡 nothing in the config yaml file")
    process.exit()
  },
  config_content => {
    console.log("😃", config_content)
    return Maybe.fromUndefined(config_content.subscribers)
  }
)

let publishers = config.cata(
  _ => {
    console.error("😡 nothing in the config yaml file")
    process.exit()
  },
  config_content => {
    console.log("😃", config_content)
    return Maybe.fromUndefined(config_content.publishers)
  }
)

subscribers.cata(
  _ => {
    console.error("😡 no subscriber")
    process.exit()
  },
  subscribers => {
    // Create MQTT Clients and Subscribe
    for(var member in subscribers) {
      let subscriber = subscribers[member]
      console.log("📡", member, subscriber)
      createMqttSubscriber(member, subscriber)
    }
  }
)


publishers.cata(
  _ => {
    console.error("😡 no publisher")
    process.exit()
  },  
  publishers => {
    // Create MQTT Clients and Timers to publish
    for(var member in publishers) {
      let publisher = publishers[member]
      console.log("🚀", member, publisher)
      createMqttPublisher(member, publisher)
    }
  }
)




// TODO:
// Logs of the data
// Send command with timer

// setInterval(function, milliseconds)